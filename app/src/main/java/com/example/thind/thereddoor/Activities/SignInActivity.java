package com.example.thind.thereddoor.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thind.thereddoor.R;
import com.example.thind.thereddoor.Responses.LoginResponse;
import com.example.thind.thereddoor.Retrofit.RestClient;
import com.example.thind.thereddoor.Util.Conventions;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener, Conventions {
    TextView notRegistered, signIn;
    EditText email, password;
    ImageButton back;
    String sEmail, sPassword;
    ProgressDialog dialog;
    Button signinbutton, forgot;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
       defination();

    }

    public void getLoginCall() {
        sEmail = email.getText().toString();
        sPassword = password.getText().toString();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignInActivity.this);
        String deviceToken = sharedPreferences.getString(DEVICE_TOKEN, "");

        new RestClient().getService().getLogin(sEmail, sPassword,
                DEVICE_TYPE,
                deviceToken, new Callback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse registerResponse, Response response) {

                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignInActivity.this);
                        SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
                        sharedEditor.putString(ACCESS_TOKEN, registerResponse.getData().getToken().toString());
                        sharedEditor.putString(FIRST_NAME, registerResponse.getData().getAgent().getName().getFirstName());
                        sharedEditor.commit();
                        Intent goToAppointments = new Intent(SignInActivity.this, YourAppointmentsActivity.class);
                        startActivity(goToAppointments);
                        finish();
                }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(SignInActivity.this, ENTER_VALID_INFO, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                ActivityCompat.finishAffinity(this);
                break;
            case R.id.signInButton:
                progressDialog();
                getLoginCall();
                String text = SIGN_UP_CODE;
                notRegistered.setText(Html.fromHtml(text));
                break;
            case R.id.forgot:
                break;
            case R.id.notRegistered:
                intent(SignUpActivity.class);
                break;
        }
    }
    private void progressDialog() {
        dialog = new ProgressDialog(SignInActivity.this);
        dialog.setMessage(LOGIN);
        dialog.show();
        sEmail = email.getText().toString();
        sPassword = password.getText().toString();
        if (sEmail.isEmpty() || sPassword.isEmpty()) {
            dialog.dismiss();
        }
    }

    private void intent(Class next) {
        Intent signup = new Intent(SignInActivity.this, next);
        startActivity(signup);
    }
    private void defination()
    {
        notRegistered = (TextView) findViewById(R.id.notRegistered);
        signinbutton = (Button) findViewById(R.id.signInButton);
        signinbutton.setOnClickListener(this);
        signIn = (TextView) findViewById(R.id.headerText);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        forgot = (Button) findViewById(R.id.forgot);
        back = (ImageButton) findViewById(R.id.back);
        back.setOnClickListener(this);
        notRegistered.setOnClickListener(this);
        signIn.setText(SIGN_IN);
        String text = REGISTERED_TEXT_CODE;
        notRegistered.setText(Html.fromHtml(text));
    }
}
