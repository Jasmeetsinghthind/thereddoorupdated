package com.example.thind.thereddoor.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thind.thereddoor.R;
import com.example.thind.thereddoor.Util.Conventions;

public class InvitationDetailsActivity extends AppCompatActivity implements View.OnClickListener,Conventions {
TextView headText;
    ImageButton back;
    ImageView map;
    Button accept,decline;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation_details);
        headText=(TextView)findViewById(R.id.headerText);
        map=(ImageView)findViewById(R.id.location_view);
        back=(ImageButton)findViewById(R.id.back);
        accept=(Button)findViewById(R.id.accept);
        decline=(Button)findViewById(R.id.decline);
        back.setOnClickListener(this);
        accept.setOnClickListener(this);
        decline.setOnClickListener(this);
        map.setVisibility(View.GONE);
    headText.setText(INVITATION_DETAILS);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back:
                intent(YourAppointmentsActivity.class);
                break;
            case R.id.accept:
                break;
            case R.id.decline:
                break;
        }
    }
    private void intent(Class next)
    {
        Intent back = new Intent(InvitationDetailsActivity.this, next);
        startActivity(back);
        finish();
    }
}
