package com.example.thind.thereddoor.Responses;

/**
 * Created by Thind on 04-11-2015.
 */
public class RegisterResponse {
    String  statusCode;
    String  error;
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
