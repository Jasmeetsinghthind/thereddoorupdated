package com.example.thind.thereddoor.Util;

/**
 * Created by Thind on 05-11-2015.
 */
public interface Conventions {
    String DEVICE_TOKEN = "Device Token";
    String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    String TYPE_AUTOCOMPLETE = "/autocomplete";
    String OUT_JSON = "/json";

    String DEVICE_TYPE = "ANDROID";
    String ACCESS_TOKEN = "Access Token";
    String EXCEPTION = "Exception";
    String UNABLE_TO_CONNECT = "Unable to connect to Geocoder";

    String SPACES = "============================================================";
    String KEY_LA = "?key=";
    String INPUT_LA = "&input=";
    String UTF_EIGHT = "utf8";
    String LATLONG = "LatLng";
    String LAT = "lat";
    String LNG = "lng", ZIP = "zip",
            CITY = "city",
            STREET = "street",
            STATE = "state",
            SUITE = "suite", TAG = "GeocodingLocation";
int ADDRESS_REQUEST_CODE=245;
    String LAT_LONG_BUNDLE = "latitude_longitude";
    String PREDICTIONS = "predictions";
    String ID = "id";
    String REFERENCE = "reference";
    String _ID = "_id";

    String CUSTOMER_LOCATION = "CUSTOMER LOCATION";
    String CHECK_INTERNET_CONNECTION = "Check your Internet Connection";
    String LOCATION = "location";
    String UNABLE_TO_FIND_ADDRESS = "Unable to find address";
    String RESULT = "result";
    String GEOMETRY = "geometry";

    String FIRST_NAME = "firstName";
    String YOUR_APPOINTMENTS = "YOUR APPOINTMENTS";
    String DATE_TYPE = "h:mm a,MMM dd,yyyy";
    String TESTING = "Testing :- ";
    String TEMP_DURATION = "Temp Duration ";
    String TEMP_SERVICE = "Temp Service ";
    String EMPTY_NOTES = "Empty Notes ";
    String TEMP_INVITE = "temp invite ";
    String ADDRESS = "U.E, Patiala City,Patiala,India ";
    String ADDRESS_LINE = "address";

    String ADDRESS_TEXT = "ADDRESS";
    String INTERNET_NO_MESSAGE = "MSG_NO_INTERNET_CONNECTION";
    String BEARER = "Bearer ";
    String NO_ADDRESS = "NO_ADDRESS_RECIEVED";
    String MSG_ADDRESS_NOT_FOUND = "MSG_ADDRESS_NOT_FOUND";

    String ABOUT_ME = "ABOUT ME";
    String ABOUT_ME_EMPTY = "About me is Empty";
    String INVITATION_DETAILS = "INVITATIONS DETAILS";

    String SCOPE = "GCM";
    String SYSTEM_ID = "832937555653";
    String SELECT_LOCATION = "SELECT LOCATION";
    String REGION = "Region";
    String SIGNUP = "Sign Up";
    String INTENT_ADDRESS = "address";
    String ENTER_VALID_INFO = "ENTER VALID INFO";
    String KEY = "AIzaSyBUHnEnus0GpYipqvo6M1QU0KbX6xj_fzQ";
    String INPUT = "input=";
    String GEOCODE = "types=geocode";
    String SENSOR_FALSE = "sensor=false";
    String JSON = "json";
    String URL = "https://maps.googleapis.com/maps/api/place/autocomplete/";
    String URL_PLACE = "https://maps.googleapis.com/maps/api/place/details/";
    String EXCEPTION_URL = "Exception url";
    String BACKGROUND_TASK = "Background Task";
    String DESCRIPTION = "description";
    String POSITION = "Position";
    String LATITUDE = "Latitude:";
    String LONGITUDE = ",Longitude:";
    String LOGIN = "Logging In";
    String SAVING = "Saving";
    String ENTER_VALUES = "Enter Values";
    String CHECK_CONNECTION = "Check your Internet Connection";
    String TRY_AGAIN = "Try Again";
    String EXIT = "Exit";
    String ADDRESS_SPLASH = "ping -c 1 www.google.com";
    int RETURN_VALUE = 0;

    int VALUE=0;
    int CASE_1=1;
    int LATLONG_0=0;
    int LATLONG_1=1;
    int LATLONG_2=2;
    int ZOOM=12;
    int SPLASH_DISPLAY_LENGTH = 1000;
    int PLACES = 0;
    int PLACES_DETAILS = 1;

    String REGISTERED_TEXT_CODE = "<font color=#ABACAD>Not Registered yet? </font>" +
            " <font color=#CD2E44><u>SIGN UP</u></font>" +
            "<font color=#ABACAD> here</font>";
    String SIGN_IN = "SIGN IN";
    String SUCCESS = "Success";
    String DEVICE_TOKEN_NULL = "Device Token Null";
    String URL_REDDOOR = "http://reddoorspa.clicklabs.in:8000";
    String SIGN_UP_CODE = "<font color=#ABACAD>Not Registered yet? </font>" +
            " <font color=#4A4A4A><u>SIGN UP</u></font>" +
            "<font color=#ABACAD> here</font>";

    String UNABLE_GEOCODER = "Unable to connect to Geocoder";

    String EMAIL_CONSTRAINT = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";//simple email regular expression
    String PHONE_CONSTRAINT = "^[1-9]-\\d{9}";//first number non zero and rest any number with length 10
    String NAME_CONSTRAINT = "^[a-zA-Z].{3,15}$";//upper case and lower case letters Length:- between 3 to 15
    String PASSWORD_CONSTRAINT = "((?=.*\\d)(?=.*[a-zA-Z]).{6,32})";//accepts numbers uppercase and lowercase alphabets and length:- between 6 to 32
    int ABOUT_ME_MIN_LENGTH = 15,
            ABOUT_ME_MAX_LENGTH = 140;
    String NULL_VALUE = "Enter Email and Password both",
            INVALID_EMAIL = "Invalid Email Id",
            INVALID_PASSWORD = "Invalid Password",
            INVALID_CONFIRM_PASSWORD = "Confirm Password doesn't match",
            INVALID_PHONE = "Invalid Phone",
            INVALID_FIRST_NAME = "Invalid First Name",
            INVALID_LAST_NAME = "Invalid Last Name",
            INVALID_EMP_ID = "Invalid Employee Id",
            INVALID_ABOUT_ME = "Length should be between 15 to 140 characters",
            INVALID_GENDER = "Select Gender",
            INVALID_REGION = "Select Gender";
}
