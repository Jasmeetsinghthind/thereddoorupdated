package com.example.thind.thereddoor.Entities;

/**
 * Created by Thind on 06-11-2015.
 */
public class RegionName {
    String _id, regionName, regionNickname, zip, __v;

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String get_id() {
        return _id;
    }

    public String getRegionName() {
        return regionName;
    }

    public String getRegionNickname() {
        return regionNickname;
    }

    public String getZip() {
        return zip;
    }

    public String get__v() {
        return __v;
    }
}
