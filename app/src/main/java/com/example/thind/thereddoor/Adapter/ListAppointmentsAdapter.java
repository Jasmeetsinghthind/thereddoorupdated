package com.example.thind.thereddoor.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thind.thereddoor.Activities.YourAppointmentsActivity;
import com.example.thind.thereddoor.Activities.CustomerLocationActivity;
import com.example.thind.thereddoor.Entities.AppointmentDetails;
import com.example.thind.thereddoor.R;
import com.example.thind.thereddoor.Util.Conventions;

import java.util.ArrayList;

/**
 * Created by Thind on 28-10-2015.
 */
public class ListAppointmentsAdapter extends BaseAdapter implements Conventions {
    private ArrayList<AppointmentDetails> appointmentDetailsArrayList;
    AppointmentDetails appointmentDetails;
    private Context mContext;
    TextView tvName, tvService, tvDuration, tvLocation, tvTime, tvTimeleft;

    ImageView ivPic,map;

    public ListAppointmentsAdapter(Context mContext, ArrayList<AppointmentDetails> appointmentDetailsArrayList) {
        this.appointmentDetailsArrayList = appointmentDetailsArrayList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return appointmentDetailsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return appointmentDetailsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater li = LayoutInflater.from(mContext);
        convertView = li.inflate(R.layout.activity_appointment_layout, parent, false);
        tvName = (TextView) convertView.findViewById(R.id.name);
        tvService = (TextView) convertView.findViewById(R.id.service);
        tvDuration = (TextView) convertView.findViewById(R.id.duration);
        tvLocation = (TextView) convertView.findViewById(R.id.location);
        tvTime = (TextView) convertView.findViewById(R.id.timeBar);
        tvTimeleft = (TextView) convertView.findViewById(R.id.timeBar1);
        ivPic = (ImageView) convertView.findViewById(R.id.image);
        map=(ImageView)convertView.findViewById(R.id.location_view);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToCustomerLocation = new Intent(mContext, CustomerLocationActivity.class);
                goToCustomerLocation.putExtra(LOCATION, appointmentDetails.getLocation());
                mContext.startActivity(goToCustomerLocation);
            }
        });
        appointmentDetails = appointmentDetailsArrayList.get(position);
        tvName.setText(appointmentDetails.getName());
        tvService.setText(appointmentDetails.getService());
        tvDuration.setText(appointmentDetails.getDuration());
        tvLocation.setText(appointmentDetails.getLocation());
        tvTime.setText(appointmentDetails.getAppointmentDate());
        tvTimeleft.setText(appointmentDetails.getInvitationDate());
        ivPic.setImageResource(appointmentDetails.getPic());
        if (YourAppointmentsActivity.seeInvitationState)
            tvTimeleft.setVisibility(View.VISIBLE);
        else
            tvTimeleft.setVisibility(View.GONE);
        return convertView;
    }
}