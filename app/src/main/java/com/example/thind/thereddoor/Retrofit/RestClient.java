package com.example.thind.thereddoor.Retrofit;

import com.example.thind.thereddoor.Util.ApiCalls;
import com.example.thind.thereddoor.Util.Conventions;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Thind on 03-11-2015.
 */
public class RestClient implements Conventions {
    ApiCalls service;
    final String BASE_URL = URL_REDDOOR;

    public RestClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(okHttpClient))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        service = restAdapter.create(ApiCalls.class);

    }

    public ApiCalls getService() {
        return service;
    }
}
