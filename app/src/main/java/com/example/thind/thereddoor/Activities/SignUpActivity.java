package com.example.thind.thereddoor.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thind.thereddoor.Adapter.GenderSpinnerAdapter;
import com.example.thind.thereddoor.Adapter.RegionSpinnerAdapter;
import com.example.thind.thereddoor.R;
import com.example.thind.thereddoor.Entities.RegionName;
import com.example.thind.thereddoor.Responses.LoginResponse;
import com.example.thind.thereddoor.Retrofit.MyResponse;
import com.example.thind.thereddoor.Retrofit.RestClient;
import com.example.thind.thereddoor.Util.Conventions;
import com.example.thind.thereddoor.Util.Validations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, Conventions {
    Button signUpButton;
    EditText firstName, lastName, email, mobile, password, confirmPassword, registeredEmployeeId;
    ImageButton back;
    ProgressDialog dialog;
    String sFirstName, sLastName, sEmail, sMobile, sPassword, sConfirmPassword, sRegisteredEmployeeId, sRegion, sGender;
    TextView signUp, member, terms;
    GenderSpinnerAdapter genderSpinnerAdapter;
    Spinner region, gender;
    Validations validate;
    ArrayList<RegionName> regions;
    RegionSpinnerAdapter regionSpinnerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        defination();
        clickListener();


        RegionName regionHint = new RegionName();
        regionHint.setRegionName(REGION);
        regions.add(regionHint);
        region.setAdapter(regionSpinnerAdapter);
        signUp.setText(SIGNUP);
        new RestClient().getService().getRegions(new Callback<MyResponse>() {
            @Override
            public void success(MyResponse myResponse, Response response) {
               /* String s = new Gson().toJson(myResponse);
                Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();*/
                regions.addAll(myResponse.getData());
                regionSpinnerAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(SignUpActivity.this, ENTER_VALID_INFO, Toast.LENGTH_SHORT).show();
            }
        });
        gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (position) {
                    case 0:
//                        gender.setText("Male");
                        break;
                    case 1:
//                        gender.setText("Female");
                        break;
                }
                String text = "<font color=#ABACAD>Already a Member?</font>" +
                        " <font color=#CD2E44><u>SIGN IN</u></font>";
                member.setText(Html.fromHtml(text));
                String text1 = "<font color=#ABACAD>you agree to</font>" +
                        " <font color=#CD2E44><u>terms of Service and Privacy</u></font>";
                terms.setText(Html.fromHtml(text1));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    public void regionsApiCall() {
        new RestClient().getService().getRegions(new Callback<MyResponse>() {


            @Override
            public void success(MyResponse myResponse, Response response) {

                regions.addAll(myResponse.getData());
                regionSpinnerAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void clickListener() {
        member.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    private void defination() {
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobile);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        registeredEmployeeId = (EditText) findViewById(R.id.registeredEmployeeId);

        region = (Spinner) findViewById(R.id.regionSpinner);
        gender = (Spinner) findViewById(R.id.genderSpinner);
        signUpButton = (Button) findViewById(R.id.signUpBtn);
        signUp = (TextView) findViewById(R.id.headerText);
        member = (TextView) findViewById(R.id.member);
        terms = (TextView) findViewById(R.id.terms);
        back = (ImageButton) findViewById(R.id.back);

        gender = (Spinner) findViewById(R.id.genderSpinner);
        genderSpinnerAdapter = new GenderSpinnerAdapter(this, getResources().getStringArray(R.array.spinnerItems));
        gender.setAdapter(genderSpinnerAdapter);

        regions = new ArrayList<>();
        validate = new Validations();
        regionSpinnerAdapter = new RegionSpinnerAdapter(this, regions);

    }


    private void intent(Class next) {
        Intent back = new Intent(SignUpActivity.this, next);
        startActivity(back);
        finish();
    }

    public void retrofitCall() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
        String deviceToken = sharedPreferences.getString(DEVICE_TOKEN, "");
        if (deviceToken != null) {
            Map<String, TypedString> params = new HashMap<>();
            params.put("firstName", new TypedString(sFirstName.toString()));
            new RestClient().getService().getRegister(new TypedString(sFirstName.toString()), new TypedString(sLastName.toString()),
                    new TypedString(sEmail.toString()), new TypedString(sRegisteredEmployeeId.toString()), new TypedString(sRegion.toString()),
                    new TypedString(sPassword.toString()), new TypedString(sMobile.toString()), new TypedString(DEVICE_TYPE.toString()),
                    new TypedString(deviceToken), new TypedString(sGender.toString()), new Callback<LoginResponse>() {
                        @Override
                        public void success(LoginResponse registerResponse, Response response) {
                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
                            SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
                            sharedEditor.putString(ACCESS_TOKEN, registerResponse.getData().getToken().toString());
                            sharedEditor.putString(FIRST_NAME, registerResponse.getData().getAgent().getName().getFirstName());
                            sharedEditor.commit();
                            intent(AboutMeActivity.class);
                            Toast.makeText(SignUpActivity.this, SUCCESS, Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(SignUpActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(SignUpActivity.this, DEVICE_TOKEN_NULL, Toast.LENGTH_SHORT).show();
    }

    public void validate() {
        dialog = new ProgressDialog(SignUpActivity.this);
        dialog.setMessage(SAVING);
        dialog.show();
        sFirstName = firstName.getText().toString();
        sLastName = lastName.getText().toString();
        sEmail = email.getText().toString();
        sMobile = mobile.getText().toString();
        sPassword = password.getText().toString();
        sConfirmPassword = confirmPassword.getText().toString();
        sRegisteredEmployeeId = registeredEmployeeId.getText().toString();
        sGender = gender.getSelectedItem().toString();
        sRegion = ((RegionName) region.getSelectedItem()).getRegionName();
        Boolean validFirstName = validate.nameValidator(sFirstName);
        Boolean validLastName = validate.nameValidator(sLastName);
        Boolean validEmail = validate.emailValidator(sEmail);
        Boolean validPhone = validate.phoneValidator(sMobile);
        Boolean validPassword = validate.passwordValidator(sPassword);
        Boolean validEmpId = validate.empIdValidator(sRegisteredEmployeeId);
        Boolean validConfirmPassword = validate.confirmPasswordValidator(sPassword, sConfirmPassword);
        if (validFirstName && validLastName && validEmail && validPassword && validConfirmPassword && validEmpId) {
            retrofitCall();
            dialog.dismiss();
        } else if (!validFirstName) {
            dialog.dismiss();
            Toast.makeText(this, INVALID_FIRST_NAME, Toast.LENGTH_SHORT).show();
        }else if (!validLastName) {
            dialog.dismiss();
            Toast.makeText(this, INVALID_LAST_NAME, Toast.LENGTH_SHORT).show();
        }else if (!validEmail) {
            dialog.dismiss();
            Toast.makeText(this, INVALID_EMAIL, Toast.LENGTH_SHORT).show();
        }else if (!validPassword) {
            dialog.dismiss();
            Toast.makeText(this, INVALID_PASSWORD, Toast.LENGTH_SHORT).show();
        }else if (!validConfirmPassword) {
            dialog.dismiss();
            Toast.makeText(this, INVALID_CONFIRM_PASSWORD, Toast.LENGTH_SHORT).show();
        }else if (!validPhone) {
            dialog.dismiss();
            Toast.makeText(this, INVALID_PHONE, Toast.LENGTH_SHORT).show();
        }else if (!validEmpId) {
            dialog.dismiss();
            Toast.makeText(this, INVALID_EMP_ID, Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.member:
                intent(SignInActivity.class);
                break;
            case R.id.back:
                intent(SignInActivity.class);
                break;
            case R.id.signUpBtn:
                validate();
                break;
        }
    }
}
