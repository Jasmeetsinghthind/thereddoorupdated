package com.example.thind.thereddoor.Maps;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import com.example.thind.thereddoor.Util.Conventions;

import java.util.HashMap;
/**
 * Created by Thind on 04-11-2015.
 */
public class CustomAutoCompleteTextView extends AutoCompleteTextView implements Conventions {

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Returns the Place Description corresponding to the selected item
     */
    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        /** Each item in the autocompetetextview suggestion list is a hashmap object */
        HashMap<String, String> hm = (HashMap<String, String>) selectedItem;
        return hm.get(DESCRIPTION);
    }
}
