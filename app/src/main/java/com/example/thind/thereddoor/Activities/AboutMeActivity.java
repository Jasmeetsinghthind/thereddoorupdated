package com.example.thind.thereddoor.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thind.thereddoor.Responses.LoginResponse;
import com.example.thind.thereddoor.R;
import com.example.thind.thereddoor.Retrofit.RestClient;
import com.example.thind.thereddoor.Util.Conventions;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AboutMeActivity extends AppCompatActivity implements View.OnClickListener,Conventions{
TextView headerText,characters;
    Button save;
    EditText info;
    ImageButton back;
    String aboutMeText;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);
        defination();
        onClick();
         }

    private final TextWatcher mTextEditorWatcher = new TextWatcher() {  // Count the total number of characters in edit text

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            characters.setText(String.valueOf(s.length() + "|" +"140"));
        }
    };

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.back:
                intent(SignUpActivity.class);
                break;
            case R.id.save:
                aboutMeText = info.getText().toString();
                dialog=new ProgressDialog(AboutMeActivity.this);
                dialog.setMessage(SAVING);
                dialog.show();
                if (!aboutMeText.isEmpty()) {
                    retrofitCallAboutMe();
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    Toast.makeText(this, ABOUT_ME_EMPTY, Toast.LENGTH_SHORT).show();
                }
                save.setBackgroundResource(R.mipmap.big_btn_pressed);
                save.setTextColor(Color.WHITE);
        }

    }
    private void intent(Class next) {
        Intent back = new Intent(AboutMeActivity.this, next);
        startActivity(back);
        finish();
    }
    public void retrofitCallAboutMe() {
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(AboutMeActivity.this);
        String token=sharedPreferences.getString(ACCESS_TOKEN,"");
        new RestClient().getService().putAboutMe(BEARER + token, aboutMeText.toString(), new Callback<LoginResponse>() {
            @Override
            public void success(LoginResponse registerResponse, Response response) {

                Intent goToAddress = new Intent(AboutMeActivity.this, AddressActivity.class);
                startActivity(goToAddress);
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(AboutMeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void onClick() {
        headerText.setText(ABOUT_ME);
        back.setOnClickListener(this);
        save.setOnClickListener(this);
    }
    private void defination()
    {
        headerText=(TextView)findViewById(R.id.headerText);
        back=(ImageButton)findViewById(R.id.back);
        save=(Button)findViewById(R.id.save);
        info=(EditText)findViewById(R.id.info);
        characters=(TextView)findViewById(R.id.characters);
        back.setOnClickListener(this);
        save.setOnClickListener(this);
        headerText.setText(ABOUT_ME);
        info.addTextChangedListener(mTextEditorWatcher);
    }
}
