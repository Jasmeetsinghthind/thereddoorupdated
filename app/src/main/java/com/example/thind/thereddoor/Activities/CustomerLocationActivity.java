package com.example.thind.thereddoor.Activities;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thind.thereddoor.Maps.ConvertLocationToLatLng;
import com.example.thind.thereddoor.R;
import com.example.thind.thereddoor.Util.Conventions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Thind on 06-11-2015.
 */
public class CustomerLocationActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener,Conventions {
    TextView headerText;
    ImageButton backButton;
    double[] latlng = new double[LATLONG_2];
    GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_location);
       defination();
        if (!googleMapAvailable()) {
            Toast.makeText(this, CHECK_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();

        } else {
            String customerLocation = getIntent().getStringExtra(LOCATION);
            ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
            convertLocationToLatLng.getLatLng(customerLocation, getApplicationContext(), new LatLngHandler());
        }
    }

    @Override
    public void onClick(View v) {
        super.onBackPressed();
    }

    private void setLocation() {
        LatLng latLng = new LatLng(latlng[LATLONG_0], latlng[LATLONG_1]);
        googleMap.addMarker(new MarkerOptions().position(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }


    public boolean googleMapAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else
            return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case CASE_1:
                    Bundle bundle = msg.getData();
                    latlng = bundle.getDoubleArray(LATLONG);
                    setLocation();
                    break;
                default:
                    Toast.makeText(CustomerLocationActivity.this, UNABLE_TO_FIND_ADDRESS, Toast.LENGTH_SHORT).show();
            }
        }
    }
private void defination()
{
    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
            .findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
    googleMap = mapFragment.getMap();
    headerText = (TextView) findViewById(R.id.headerText);
    headerText.setText(CUSTOMER_LOCATION);
    backButton = (ImageButton) findViewById(R.id.back);
    backButton.setOnClickListener(this);
}
}