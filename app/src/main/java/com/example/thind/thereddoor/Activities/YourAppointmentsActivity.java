package com.example.thind.thereddoor.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.thind.thereddoor.Entities.AppointmentDetails;
import com.example.thind.thereddoor.Adapter.ListAppointmentsAdapter;
import com.example.thind.thereddoor.R;
import com.example.thind.thereddoor.Util.Conventions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class YourAppointmentsActivity extends AppCompatActivity implements View.OnClickListener,Conventions {
    TextView appointmentHead;
    ImageButton back,menu,menuPressed;
    Button scheduled,invitations;
    ArrayAdapter navigationAdapter;
    private ListView mDrawerList,list;
    RelativeLayout navigationRelative;
    TextView navDrawerId;
    SharedPreferences sharedPreferences;
    ListAppointmentsAdapter listAdapter;
    private ArrayAdapter<String> mAdapter;
    ArrayList<AppointmentDetails> appointmentDetailsArrayList=new ArrayList<>();
    private DrawerLayout mDrawerLayout;
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    public static boolean seeInvitationState;
    public static boolean status=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_appointments);
        navigationRelative=(RelativeLayout)findViewById(R.id.navigationRelative);
        appointmentHead=(TextView)findViewById(R.id.headerText);
        navDrawerId=(TextView)findViewById(R.id.navigationName);
        mDrawerList = (ListView) findViewById(R.id.navList);
        scheduled=(Button)findViewById(R.id.scheduled);
        invitations=(Button)findViewById(R.id.invitations);
        back=(ImageButton)findViewById(R.id.back);
//        navigationAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.menu));
        list=(ListView)findViewById(R.id.list);

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);

        mDrawerList.setAdapter(navigationAdapter);
        list.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                intent(InvitationDetailsActivity.class);
            }
        });
        listAdapter=new ListAppointmentsAdapter(this,appointmentDetailsArrayList);
        list.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
        createList();
        menu=(ImageButton)findViewById(R.id.menu);
        menuPressed=(ImageButton)findViewById(R.id.menuPressed);
        appointmentHead.setText(YOUR_APPOINTMENTS);
        back.setVisibility(View.GONE);


        menu.setOnClickListener(this);
        menuPressed.setOnClickListener(this);
        scheduled.setOnClickListener(this);
        invitations.setOnClickListener(this);
       navigationDrawer();
    }

    private void setupDrawer() {
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }
    private void addDrawerItems() {
        String[] osArray = {"HOME","MY ACCOUNT","OPEN APPOINTMENT","ABOUT ME","BOOKING HISTORY","RAISE ALARM","LOGOUT",};
        mAdapter = new ArrayAdapter<String>(YourAppointmentsActivity.this, android.R.layout.simple_list_item_1, osArray);

    }

    private void navigationDrawer()
    {
        addDrawerItems();
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> mAdapterView, View mDrawerList, int position, long id) {
                switch (position) {
                case 6:
                intent(SignInActivity.class);
                 break;
                }
            }
        });
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                menu.setVisibility(View.INVISIBLE);
                menuPressed.setVisibility(View.VISIBLE);
                String nameDrawer=sharedPreferences.getString(FIRST_NAME," ");
                navDrawerId.setText(nameDrawer);
            }
            public void onDrawerClosed(View view) {
                menu.setVisibility(View.VISIBLE);
                menuPressed.setVisibility(View.INVISIBLE);
                super.onDrawerClosed(view);
                }
        };
        setupDrawer();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.scheduled:
                scheduled.setBackgroundResource(R.mipmap.small_btn_pressed);
                invitations.setBackgroundResource(R.mipmap.small_btn_normal);
                status=false;
                scheduled.setTextColor(Color.WHITE);
                invitations.setTextColor(Color.BLACK);
                seeInvitationState=false;
                listAdapter.notifyDataSetChanged();
                break;
            case R.id.invitations:
                invitations.setBackgroundResource(R.mipmap.small_btn_pressed);
                scheduled.setBackgroundResource(R.mipmap.small_btn_normal);
                status=true;
                scheduled.setTextColor(Color.BLACK);
                invitations.setTextColor(Color.WHITE);
                seeInvitationState=true;
                listAdapter.notifyDataSetChanged();
                break;
            case R.id.location_view:
                intent(MapsLocationActivity.class);
                break;
        }
    }
    private void intent(Class next)
    {
        Intent signup = new Intent(YourAppointmentsActivity.this, next);
        startActivity(signup);
        finish();
    }
    private void createList()
    {
        AppointmentDetails appointment;
        for(int i=0;i<10;i++)
        {
            SimpleDateFormat form=new SimpleDateFormat(DATE_TYPE);
            appointment=new AppointmentDetails();
            appointment.setAppointmentDate(form.format(new Date()));
            appointment.setAppointmentDate(form.format(new Date()));
            appointment.setPic(R.mipmap.small_placeholder);
            appointment.setName(TESTING + i);
            appointment.setDuration(TEMP_DURATION + i);
            appointment.setService(TEMP_SERVICE + i);
            appointment.setCustomerNotes(EMPTY_NOTES + i);
            appointment.setInvitationDate(TEMP_INVITE + i);
            appointment.setLocation(ADDRESS);
            appointmentDetailsArrayList.add(appointment);
        }
    }
}
