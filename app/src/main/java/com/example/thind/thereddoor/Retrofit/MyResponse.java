package com.example.thind.thereddoor.Retrofit;

import com.example.thind.thereddoor.Entities.RegionName;

import java.util.ArrayList;

/**
 * Created by Thind on 03-11-2015.
 */
public class MyResponse {
    String message;
    ArrayList<RegionName> data;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<RegionName> getData() {
        return data;
    }

    public void setData(ArrayList<RegionName> data) {
        this.data = data;
    }
}
