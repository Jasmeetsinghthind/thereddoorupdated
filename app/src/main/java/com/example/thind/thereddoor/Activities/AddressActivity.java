package com.example.thind.thereddoor.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thind.thereddoor.Maps.ConvertLocationToLatLng;
import com.example.thind.thereddoor.Responses.LoginResponse;
import com.example.thind.thereddoor.R;
import com.example.thind.thereddoor.Retrofit.RestClient;
import com.example.thind.thereddoor.Util.Conventions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddressActivity extends AppCompatActivity implements View.OnClickListener, Conventions {
    TextView headerText;
    EditText etStreet, etCity, etSuite, etZip, etState;
    String street, suite, city, state, zip;
    ImageButton back;
    Button save;
    ProgressDialog dialog;
    RelativeLayout mapClick;
    double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        defination();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                intent(AboutMeActivity.class);
                break;
            case R.id.save:
                getTextFunction();
                dialog();
                if (zip.isEmpty() || city.isEmpty() || street.isEmpty() || suite.isEmpty()) {
                    dialog.dismiss();
                }
                validate();
                save.setBackgroundResource(R.mipmap.big_btn_pressed);
                save.setTextColor(Color.WHITE);
                break;
            case R.id.searchLayout:
                Intent intent = new Intent(AddressActivity.this, MapsLocationActivity.class);
                startActivityForResult(intent, ADDRESS_REQUEST_CODE);
                break;
        }
    }

    public void validate() {
        getTextFunction();
        if (zip != null && !zip.isEmpty()
                && city != null && !city.isEmpty()
                && street != null && !street.isEmpty()
                && state != null && !state.isEmpty()
                && suite != null && !suite.isEmpty()) {
            if (lat == 0.0 || lng == 0.0) {
                String agentAddress = suite + " " + street + " " + city + " " + state;
                int status = isGooglePlayServicesAvailable();
                if (status != ConnectionResult.SUCCESS) {
                    Toast.makeText(AddressActivity.this, INTERNET_NO_MESSAGE, Toast.LENGTH_SHORT).show();
                } else {
                    ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
                    convertLocationToLatLng.convertToAddressDetails(agentAddress, getApplicationContext(), new LatLngHandler());
                }
            } else {
                callRest();
            }
        } else
            Toast.makeText(AddressActivity.this, ENTER_VALUES, Toast.LENGTH_SHORT).show();
    }

    private void getTextFunction() {

        zip = etZip.getText().toString();
        city = etCity.getText().toString();
        street = etStreet.getText().toString();
        state = etState.getText().toString();
        suite = etSuite.getText().toString();
    }

    private void intent(Class next) {
        Intent back = new Intent(AddressActivity.this, next);
        startActivity(back);
        finish();
    }

    public void callRest() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AddressActivity.this);
        String token = sharedPreferences.getString(ACCESS_TOKEN, "");
        new RestClient().getService().postAddress(BEARER + token, street, suite, city, state, zip, lat, lng
                , new Callback<LoginResponse
                >() {
            @Override
            public void success(LoginResponse apiUserResponse, Response response) {
                startActivity(new Intent(AddressActivity.this, YourAppointmentsActivity.class));
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(AddressActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private int isGooglePlayServicesAvailable() {
        //checking the google services(Internet Connection)
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        return status;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == ADDRESS_REQUEST_CODE) && (resultCode == RESULT_OK)) {
            String agentAddress = data.getStringExtra(ADDRESS_LINE);
            int status = isGooglePlayServicesAvailable();
            if (agentAddress != null) {
                if (status == ConnectionResult.SUCCESS) {
                    ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
                    convertLocationToLatLng.convertToAddressDetails(agentAddress, getApplicationContext(), new AddressHandler());
                } else
                    Toast.makeText(AddressActivity.this, INTERNET_NO_MESSAGE, Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(AddressActivity.this, NO_ADDRESS, Toast.LENGTH_SHORT).show();
        }
    }

    public class AddressHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            switch (msg.what) {
                case 1:
                    String city = bundle.getString(CITY),

                            street = bundle.getString(STREET), state = bundle.getString(STATE), zip = bundle.getString(ZIP);
                    if (city != null)
                        etCity.setText(city);
                    if (zip != null)
                        etZip.setText(zip);
                    if (state != null)
                        etState.setText(state);
                    if (street != null)
                        etStreet.setText(street);

                    double[] latLong;
                    latLong = bundle.getDoubleArray(LAT_LONG_BUNDLE);
                    lat = latLong[LATLONG_0];
                    lng = latLong[LATLONG_1];
                    break;
                case VALUE:
                default:
                    Toast.makeText(AddressActivity.this, MSG_ADDRESS_NOT_FOUND, Toast.LENGTH_SHORT).show();
            }
        }
    }

    // to find only LatLong
    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            double[] latLong = bundle.getDoubleArray(LAT_LONG_BUNDLE);

            switch (msg.what) {
                case 1:
                    lat = latLong[LATLONG_0];
                    lng = latLong[LATLONG_1];
                    callRest();
                    break;
                case 0:
                default:
                    Toast.makeText(AddressActivity.this, MSG_ADDRESS_NOT_FOUND, Toast.LENGTH_SHORT).show();
            }
        }
    }
        private void dialog()
        {
            dialog = new ProgressDialog(this);
            dialog.setMessage(LOGIN);
            dialog.show();
        }

    private void defination() {
        headerText = (TextView) findViewById(R.id.headerText);
        back = (ImageButton) findViewById(R.id.back);
        mapClick = (RelativeLayout) findViewById(R.id.searchLayout);
        back.setOnClickListener(this);
        mapClick.setOnClickListener(this);
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(this);
        headerText.setText(ADDRESS_TEXT);
        etStreet = (EditText) findViewById(R.id.street);
        etCity = (EditText) findViewById(R.id.city);
        etSuite = (EditText) findViewById(R.id.apartment);
        etZip = (EditText) findViewById(R.id.zip);
        etState = (EditText) findViewById(R.id.state);
    }
}
